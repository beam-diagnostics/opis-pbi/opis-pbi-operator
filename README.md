# opis-pbi-operator

For management of the EPICS OPIs used primarily by operators in the ESS control room(s) for PBI functions.

Repository admins: Thomas Fay, Joao-Paulo Martins

## Workflow  
Addition of content is triggered by end-user need. Consequently content is generally added on a per-system basis. This repository is to hold the OPIs utilised by control room operator end-users of such OPIs and make these OPIs available in the ESS control room(s). Secondary users of these OPIs can of course exist but the OPIs are optimised towards their primary use-case of control room operators.  This repository is not a development environment. OPI authors are responsible for developing and managing their own source history external to this repository.  
Content addition to this repository is subject to three formalised criteria:  
1) End-users are given a chance to test the new content and approve it for usage in the control room(s). Approval criteria are checking the OPI is true representation of the system, the OPI is error-free and the OPI is usable for its intended purpose. Missing functionality is not normally a reason to delay an otherwise useful OPI being released for control room usage. Such additional functionality can be noted and included in a next release of the OPI content. In the PBI context end-users are considered to be Elena Donegani and Benjamin Bolling who should both provide feedback prior to OPIs being released to control room. Elena is free to delegate approval to any PBI staff, for example to a specific system lead. Benjamin is free to delegate approval to any Operators group member.  
The author of the new content is to create a JIRA ticket in PBITECH project wth label 'CR-OPI' and assign to each reviewer in turn to get feedback. Once both reviewers are ok for release the merge request can be created and assigned to a repo admin. An OPI author cannot act as a reviewer in this workflow and therefore is to delegate their approval role if necessary to a colleague in their group who will be an end-user of the OPI.    
2) ICS Graphical Designer (Dirk Nordt) provides feedback on the PBITECH ticket regarding aesthetics of the OPI and coherence with displays from other disciplines. Again Dirk can delegate to a representative from ICS Software group as appropriate.
3) Repo admins (Thomas Fay/Joao-Paulo Martins) make a final (secondary) check of the new content to ensure house-rules for the repository are followed. See below. They also ensure that feedback from the end-users and ICS Graphic Designer have been captured on a JIRA ticket in PBITECH project.  

## Mechanics
Any OPI author can add content by forking this repository, adding content and submitting a merge request back to the upstream repository. Repo admins can help here if required.


## House-Rules and Conventions  
Three directories are provided for structural reasons in this repository:  
00-Overview  
10-Top   
99-Shared  

00-Overview is to contain OPI displays that contain summary information for the entire PBI discipline.  
10-Top is intended to contain OPI displays that can be opened directly in the OPI runtime by the users.  
99-Shared is intended for OPI components that are intended to be leveraged by "top-level" OPIs, for example plots, embedded widgets etc.  

Things to be avoided:  
Submitting content for multiple systems in one approval workflow. This is because the approval needs to be handled in a lean manner with only those using the new OPI content involved.  
Using this repository as a development tool. This is because this repository is intended to provde a formal means for includig approval OPIs in the control room(s).  
Tracking development history. For the same reasons as above. Therefore additions such as BCM_v2, BPM_old etc. are not be included in the OPIs visible from the control room.  
